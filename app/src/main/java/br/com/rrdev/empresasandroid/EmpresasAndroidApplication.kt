package br.com.rrdev.empresasandroid

import android.app.Application
import br.com.rrdev.empresasandroid.di.module.AppModule
import br.com.rrdev.empresasandroid.di.module.PreferenceModule
import br.com.rrdev.empresasandroid.di.module.UseCasesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EmpresasAndroidApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@EmpresasAndroidApplication)
            modules(listOf(AppModule, PreferenceModule, UseCasesModule))
        }
    }
}