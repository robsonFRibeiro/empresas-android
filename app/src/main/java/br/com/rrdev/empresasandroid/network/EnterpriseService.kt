package br.com.rrdev.empresasandroid.network

import br.com.rrdev.empresasandroid.BuildConfig
import br.com.rrdev.empresasandroid.BuildConfig.*
import br.com.rrdev.empresasandroid.data.models.Enterprise
import br.com.rrdev.empresasandroid.data.models.EnterprisesDataWrapper
import br.com.rrdev.empresasandroid.data.models.LogInBody
import br.com.rrdev.empresasandroid.data.models.LogInDataWrapper
import retrofit2.Call
import retrofit2.http.*

interface EnterpriseService {

    @POST("users/auth/sign_in")
    fun logIn(@Body body: LogInBody): Call<LogInDataWrapper>

    @GET("enterprises")
    fun getAllEnterprises(
        @Header(ACCESS_TOKEN_HEADER_KEY) accessToken: String,
        @Header(CLIENT_HEADER_KEY) client: String,
        @Header(UID_HEADER_KEY) uid: String,
    ): Call<EnterprisesDataWrapper>

    @GET("enterprises")
    fun getEnterprisesByName(
        @Header(ACCESS_TOKEN_HEADER_KEY) accessToken: String,
        @Header(CLIENT_HEADER_KEY) client: String,
        @Header(UID_HEADER_KEY) uid: String,
        @Query("enterprise_types") type: Int,
        @Query("name") name: String
    ): Call<EnterprisesDataWrapper>

}