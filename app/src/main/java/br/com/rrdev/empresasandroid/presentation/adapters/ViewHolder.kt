package br.com.rrdev.empresasandroid.presentation.adapters

import android.content.SharedPreferences
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.rrdev.empresasandroid.BuildConfig
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.data.models.Enterprise
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import kotlinx.android.synthetic.main.adapter_enterprise_layout.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class ViewHolder(
    private val view: View,
    private val onItemSelect: ((Int) -> Unit)? = null
): RecyclerView.ViewHolder(view), KoinComponent {

    private val sharedPreferences: SharedPreferences by inject()

    init {
        view.setOnClickListener { onItemSelect?.invoke(adapterPosition) }
    }

    fun bindContent(enterprise: Enterprise){
        view.run{
            val imgUrl = "${BuildConfig.BASE_URL}${enterprise.photo}"
            val glideUrl = GlideUrl(imgUrl){ mapOf(
                BuildConfig.ACCESS_TOKEN_HEADER_KEY to sharedPreferences.getString(BuildConfig.ACCESS_TOKEN_HEADER_KEY, ""),
                BuildConfig.CLIENT_HEADER_KEY to sharedPreferences.getString(BuildConfig.CLIENT_HEADER_KEY, ""),
                BuildConfig.UID_HEADER_KEY to sharedPreferences.getString(BuildConfig.UID_HEADER_KEY, ""),
            )}

            Glide.with(view)
                .load(glideUrl)
                .placeholder(R.drawable.logo_home)
                .error(R.drawable.logo_home)
                .into(img_logo)

            text_name.text = enterprise.enterprise_name
            text_content.text = enterprise.enterprise_type.enterprise_type_name
            text_location.text = enterprise.country
        }
    }
}