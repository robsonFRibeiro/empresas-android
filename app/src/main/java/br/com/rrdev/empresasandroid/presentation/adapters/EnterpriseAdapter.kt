package br.com.rrdev.empresasandroid.presentation.adapters

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.rrdev.empresasandroid.BuildConfig
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.data.models.Enterprise
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import kotlinx.android.synthetic.main.adapter_enterprise_layout.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class EnterpriseAdapter: RecyclerView.Adapter<ViewHolder>() {

    var onItemSelect: ((Int) -> Unit)? = null
    var enterpriseList = emptyList<Enterprise>()
        set(value) {
            val diffUtil = DiffUtil.calculateDiff(EnterpriseDiffUtil(enterpriseList, value))
            diffUtil.dispatchUpdatesTo(this)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.adapter_enterprise_layout, parent, false), onItemSelect)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindContent(enterpriseList[position])

    override fun getItemCount(): Int = enterpriseList.size

    fun getItemByPosition(position: Int) = enterpriseList[position]

}