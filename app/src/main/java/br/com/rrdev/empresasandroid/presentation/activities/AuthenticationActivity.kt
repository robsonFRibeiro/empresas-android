package br.com.rrdev.empresasandroid.presentation.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.rrdev.empresasandroid.IdlingResource.EspressoIdlingResource
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.presentation.dialogs.LoadDialog
import br.com.rrdev.empresasandroid.presentation.states.Result
import br.com.rrdev.empresasandroid.presentation.viewmodels.AuthenticationViewModel
import kotlinx.android.synthetic.main.login_layout.*
import org.koin.android.viewmodel.ext.android.viewModel

class AuthenticationActivity: AppCompatActivity(R.layout.login_layout) {

    private val viewModel: AuthenticationViewModel by viewModel()
    private val loadDialog = LoadDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModel()

        button_confirm.setOnClickListener {
            text_message.visibility = View.GONE
            EspressoIdlingResource.increment()
            viewModel.logIn(edit_text_email.text.toString(), edit_text_password.text.toString())
        }

    }

    private fun initViewModel() = viewModel.logInData.observe(this, { result ->
        when(result){
            is Result.InProgress ->{
                loadDialog.show(supportFragmentManager, null)
            }
            is Result.Error ->{
                loadDialog.dismiss()
                text_message.text = getString(result.errorMessage)
                text_message.visibility = View.VISIBLE
                EspressoIdlingResource.decrement()
            }
            is Result.Success<*> -> {
                loadDialog.dismiss()
                startEnterpriseListActivity()
            }
        }
    })

    private fun startEnterpriseListActivity(){
        startActivity(Intent(applicationContext, EnterpriseListActivity::class.java))
        finish()
    }

}