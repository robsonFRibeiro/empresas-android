package br.com.rrdev.empresasandroid.domain.usecase

import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.presentation.states.Result

class LogInUseCase(private val enterpriseRepository: EnterpriseRepository) {

    suspend fun logIn(email:String, password: String): Result =
        enterpriseRepository.logIn(email, password)
}