package br.com.rrdev.empresasandroid.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseType(
    val enterprise_type_name: String,
    val id: Int
): Parcelable