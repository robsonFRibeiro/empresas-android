package br.com.rrdev.empresasandroid.data.repository

import android.content.SharedPreferences
import br.com.rrdev.empresasandroid.BuildConfig.*
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.data.models.LogInBody
import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.network.EnterpriseService
import br.com.rrdev.empresasandroid.presentation.states.Result

class EnterpriseRepositoryImp(
    private val enterpriseService: EnterpriseService,
    private val sharedPreferences: SharedPreferences
): EnterpriseRepository {

    override suspend fun logIn(email: String, password: String): Result {
        try {

            val response = enterpriseService.logIn(LogInBody(email, password)).execute()
            if (response.isSuccessful){
                val headers = response.headers()
                with(sharedPreferences.edit()){
                    putString(ACCESS_TOKEN_HEADER_KEY, headers[ACCESS_TOKEN_HEADER_KEY])
                    putString(CLIENT_HEADER_KEY, headers[CLIENT_HEADER_KEY])
                    putString(UID_HEADER_KEY, headers[UID_HEADER_KEY])
                    commit()
                }
                return Result.Success(data = response.body())
            }
            return Result.Error(ErrorFormatFactory.getMessageForCode(response.code()))

        }catch (e: Exception){
            return Result.Error(ErrorFormatFactory.getMessageForText(e.message))
        }
    }

    @Throws(Exception::class)
    override suspend fun getAllEnterprises(): Result {
        try {

            val response = enterpriseService.getAllEnterprises(
                sharedPreferences.getString(ACCESS_TOKEN_HEADER_KEY, null) ?: throw Exception("Access Token is null"),
                sharedPreferences.getString(CLIENT_HEADER_KEY, null) ?: throw Exception("Client is null"),
                sharedPreferences.getString(UID_HEADER_KEY,null) ?: throw Exception("UID is null"),
            ).execute()

            if (response.isSuccessful)
                return Result.Success(response.body())

            return Result.Error(ErrorFormatFactory.getMessageForCode(response.code()))
        }catch (e: Exception){
            return Result.Error(ErrorFormatFactory.getMessageForText(e.message))
        }
    }

    override suspend fun searchEnterpriseByName(name: String): Result {
        try {

            val response = enterpriseService.getEnterprisesByName(
                sharedPreferences.getString(ACCESS_TOKEN_HEADER_KEY, null) ?: throw Exception("Access Token is null"),
                sharedPreferences.getString(CLIENT_HEADER_KEY, null) ?: throw Exception("Client is null"),
                sharedPreferences.getString(UID_HEADER_KEY,null) ?: throw Exception("UID is null"),
                type = 1,
                name = name
            ).execute()

            if (response.isSuccessful)
                return Result.Success(response.body())

            return Result.Error(ErrorFormatFactory.getMessageForCode(response.code()))
        }catch (e: Exception){
            return Result.Error(ErrorFormatFactory.getMessageForText(e.message))
        }
    }
}