package br.com.rrdev.empresasandroid.domain.usecase

import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.presentation.states.Result

class SearchEnterpriseUseCase(private val enterpriseRepository: EnterpriseRepository) {

    suspend fun searchEnterpriseByName(enterpriseName: String): Result =
        enterpriseRepository.searchEnterpriseByName(enterpriseName)
}