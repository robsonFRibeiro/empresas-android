package br.com.rrdev.empresasandroid.presentation.viewmodels

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.rrdev.empresasandroid.domain.usecase.LogInUseCase
import br.com.rrdev.empresasandroid.presentation.states.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AuthenticationViewModel(
    private val logInUseCase: LogInUseCase
): ViewModel() {

    val logInData = MutableLiveData<Result>()

    fun logIn(email: String, password: String) = viewModelScope.launch{
        logInData.value = Result.InProgress
        logInData.value = withContext(Dispatchers.IO){
            logInUseCase.logIn(email, password)
        }
    }
}