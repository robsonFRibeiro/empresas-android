package br.com.rrdev.empresasandroid.domain.repository

import br.com.rrdev.empresasandroid.presentation.states.Result

interface EnterpriseRepository {

    suspend fun logIn(email: String, password: String): Result

    suspend fun getAllEnterprises(): Result

    suspend fun searchEnterpriseByName(name: String): Result
}