package br.com.rrdev.empresasandroid.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import br.com.rrdev.empresasandroid.BuildConfig
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val PreferenceModule = module {
    single { createPreferences(androidApplication()) }
}

fun createPreferences(application: Application): SharedPreferences {
    return application.getSharedPreferences(BuildConfig.PREFERENCES_KEY, Context.MODE_PRIVATE)
}
