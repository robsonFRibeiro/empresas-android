package br.com.rrdev.empresasandroid.di.module

import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.domain.usecase.ListAllEnterprisesUseCase
import br.com.rrdev.empresasandroid.domain.usecase.LogInUseCase
import br.com.rrdev.empresasandroid.domain.usecase.SearchEnterpriseUseCase
import org.koin.dsl.module

val UseCasesModule = module {

    single { createLogInUseCase(get()) }

    single { createListAllEnterprisesUseCase(get()) }

    single { createSearchEnterpriseUseCase(get()) }

}

fun createLogInUseCase(enterpriseRepository: EnterpriseRepository): LogInUseCase =
    LogInUseCase(enterpriseRepository)

fun createListAllEnterprisesUseCase(enterpriseRepository: EnterpriseRepository): ListAllEnterprisesUseCase =
    ListAllEnterprisesUseCase(enterpriseRepository)

fun createSearchEnterpriseUseCase(enterpriseRepository: EnterpriseRepository): SearchEnterpriseUseCase =
    SearchEnterpriseUseCase(enterpriseRepository)