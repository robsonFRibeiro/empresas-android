package br.com.rrdev.empresasandroid.presentation.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.rrdev.empresasandroid.IdlingResource.EspressoIdlingResource
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.data.models.EnterprisesDataWrapper
import br.com.rrdev.empresasandroid.presentation.adapters.EnterpriseAdapter
import br.com.rrdev.empresasandroid.presentation.dialogs.LoadDialog
import br.com.rrdev.empresasandroid.presentation.states.Result
import br.com.rrdev.empresasandroid.presentation.viewmodels.EnterpriseListViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.layout_recycler_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class EnterpriseListActivity : AppCompatActivity() {

    private lateinit var adapter: EnterpriseAdapter
    private val loadDialog = LoadDialog()
    private val viewModel: EnterpriseListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_recycler_list)

        setSupportActionBar(toolbar)

        initAdapter()
        initViewModel()
        viewModel.getEnterprises()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar_list, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val menuItem = menu?.findItem(R.id.action_menu_toolbar_search)
        menuItem?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                viewModel.getEnterprises()
                return true
            }
        })
        (menuItem?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return true
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent?.action != Intent.ACTION_SEARCH){
            super.onNewIntent(intent)
        }
        val query = intent?.getStringExtra(SearchManager.QUERY)
        query?.let { viewModel.getEnterprises(it) }

    }

    private fun initViewModel() = viewModel.enterprisesData.observe(this, { result ->
        when(result){
            is Result.InProgress ->{
                loadDialog.show(supportFragmentManager, null)
            }
            is Result.Error ->{
                loadDialog.dismiss()
                showSnackBar(getString(result.errorMessage))
            }
            is Result.Success<*> -> {
                loadDialog.dismiss()
                adapter.enterpriseList = (result.data as EnterprisesDataWrapper).enterprises
                if (adapter.itemCount <= 0){
                    recycler_view.visibility = View.GONE
                    text_message.visibility = View.VISIBLE
                }else{
                    recycler_view.visibility = View.VISIBLE
                    text_message.visibility = View.GONE
                }
            }
        }
    })

    private fun initAdapter() {
        if (::adapter.isInitialized.not()){
            adapter = EnterpriseAdapter().apply {
                onItemSelect = ::handleItemClick
            }
            recycler_view.layoutManager = LinearLayoutManager(applicationContext)
            recycler_view.adapter = adapter
        }
    }

    private fun handleItemClick(position: Int){
        val enterprise = adapter.getItemByPosition(position)
        EnterpriseDetailActivity.newActivityInstance(this, enterprise)
    }

    private fun showSnackBar(message: String){
        val snackbar = Snackbar
            .make(parent_list, message, Snackbar.LENGTH_LONG)
            .setAction(getString(R.string.try_again)) { viewModel.getEnterprises() }
            (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView).maxLines = 10
        snackbar.show()
    }
}