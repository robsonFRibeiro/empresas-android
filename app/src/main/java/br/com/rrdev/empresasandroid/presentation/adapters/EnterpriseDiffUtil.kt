package br.com.rrdev.empresasandroid.presentation.adapters

import androidx.recyclerview.widget.DiffUtil
import br.com.rrdev.empresasandroid.data.models.Enterprise

class EnterpriseDiffUtil(private val oldList: List<Enterprise>, private val newList: List<Enterprise>): DiffUtil.Callback() {
    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].hashCode() == newList[newItemPosition].hashCode()
    }
}