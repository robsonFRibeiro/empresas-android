package br.com.rrdev.empresasandroid.data.models

data class LogInBody(val email: String, val password: String)