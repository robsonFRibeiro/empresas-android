package br.com.rrdev.empresasandroid.presentation.viewmodels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.rrdev.empresasandroid.domain.usecase.ListAllEnterprisesUseCase
import br.com.rrdev.empresasandroid.domain.usecase.SearchEnterpriseUseCase
import br.com.rrdev.empresasandroid.presentation.states.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EnterpriseListViewModel(
    private val listAllEnterprisesUseCase: ListAllEnterprisesUseCase,
    private val searchEnterpriseUseCase: SearchEnterpriseUseCase
): ViewModel() {

    private val allEnterprises = MutableLiveData<Result>()
    private val searchEnterprises = MutableLiveData<Result>()

    val enterprisesData = MediatorLiveData<Result>()

    private var searchContent: String? = null

    init {
        enterprisesData.addSource(allEnterprises) { result: Result? ->
            if (searchContent.isNullOrEmpty()) {
                result?.let { enterprisesData.value = it }
            }
        }
        enterprisesData.addSource(searchEnterprises) { result: Result? ->
            if (searchContent.isNullOrEmpty().not()){
                result?.let { enterprisesData.value = it }
            }
        }
    }


    fun getEnterprises(enterpriseName: String? = null) = viewModelScope.launch{
        searchContent = enterpriseName
        enterprisesData.value = Result.InProgress
        if (searchContent.isNullOrEmpty()){
            enterprisesData.value = withContext(Dispatchers.IO){
                listAllEnterprisesUseCase.getAllEnterprises()
            }
        }else{
            enterprisesData.value = withContext(Dispatchers.IO){
                searchEnterpriseUseCase.searchEnterpriseByName(searchContent!!)
            }
        }
    }
}