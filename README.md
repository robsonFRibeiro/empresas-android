![N|Solid](logo_ioasys.png)

# README #

Este documento README tem como objetivo fornecer as informações sobre o projeto desafio para vaga de desenvolvedor Android.

### Executando o projeto ###

* Clonar repositório
* Abrir pasta do projeto com [Android Studio](https://developer.android.com/studio/run?hl=pt-br)

### Dependências ###

* Glide
    * Biblioteca usada para carregamentos de imagens fornecidos pela API

* Kotlin Coroutines
    * Utilizado para tarefas assíncronas, tal como acessar os dados do servidor via API

* LifeCycle Componentes
    * Para o controle de ciclo de vida das activities;
    * Implementação da arquitetura MVVM;
    * Observar alterações nas informações do ViewModel
    
 * Koin
    * Injeção de dependências
 
 * Material Componentes
    * Para aplicação do material design


### Intenções de implementações ###

* Minificar/Obscurer o código com ProGuard R8
* Gerenciamento e controle sessões de usuários
* Conteúdo salvo no banco de dados local, com utilização do Room 