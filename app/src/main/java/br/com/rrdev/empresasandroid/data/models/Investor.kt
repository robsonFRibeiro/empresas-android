package br.com.rrdev.empresasandroid.data.models

data class Investor(
    val balance: Double,
    val city: String,
    val country: String,
    val email: String,
    val first_access: Boolean,
    val id: Int,
    val investor_name: String,
    val photo: String,
    val portfolio: Portfolio,
    val portfolio_value: Double,
    val super_angel: Boolean
)