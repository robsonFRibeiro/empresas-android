package br.com.rrdev.empresasandroid.domain.usecase

import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.presentation.states.Result

class ListAllEnterprisesUseCase(private val enterpriseRepository: EnterpriseRepository) {

    suspend fun getAllEnterprises(): Result =
        enterpriseRepository.getAllEnterprises()
}