package br.com.rrdev.empresasandroid.presentation.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.rrdev.empresasandroid.BuildConfig
import br.com.rrdev.empresasandroid.BuildConfig.*
import br.com.rrdev.empresasandroid.R
import br.com.rrdev.empresasandroid.data.models.Enterprise
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import kotlinx.android.synthetic.main.layout_detail_enterprise.*
import org.koin.android.ext.android.inject

class EnterpriseDetailActivity : AppCompatActivity() {

    private lateinit var enterprise: Enterprise
    private val sharedPreferences: SharedPreferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_detail_enterprise)

        enterprise = intent?.getParcelableExtra(PARCELABLE_KEY) ?: throw Exception("Enterprise can't be null")

        fillContent()

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun fillContent() {
        toolbar.title = enterprise.enterprise_name
        text_description.text = enterprise.description

        val imgUrl = "$BASE_URL${enterprise.photo}"
        val glideUrl = GlideUrl(imgUrl){ mapOf(
            ACCESS_TOKEN_HEADER_KEY to sharedPreferences.getString(ACCESS_TOKEN_HEADER_KEY, ""),
            CLIENT_HEADER_KEY to sharedPreferences.getString(CLIENT_HEADER_KEY, ""),
            UID_HEADER_KEY to sharedPreferences.getString(UID_HEADER_KEY, ""),
        )}

        Glide.with(this)
            .load(glideUrl)
            .placeholder(R.drawable.logo_home)
            .error(R.drawable.logo_home)
            .into(img_logo)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }


    companion object{
        private const val PARCELABLE_KEY = "ENTERPRISE_KEY"

        fun newActivityInstance(context: Context, enterprise: Enterprise) {
            val intent = Intent(context, EnterpriseDetailActivity::class.java).apply {
                putExtra(PARCELABLE_KEY, enterprise)
            }
            context.startActivity(intent)
        }
    }

}