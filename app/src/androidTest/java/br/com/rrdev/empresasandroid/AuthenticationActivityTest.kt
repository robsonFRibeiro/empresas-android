package br.com.rrdev.empresasandroid


import android.content.Intent
import android.view.InputDevice
import android.view.MotionEvent
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import br.com.rrdev.empresasandroid.IdlingResource.EspressoIdlingResource
import br.com.rrdev.empresasandroid.presentation.activities.AuthenticationActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matcher
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
@LargeTest
class AuthenticationActivityTest {

    private val emailToBeTyped = "testeapple@ioasys.com.br"
    private val wrongPasswordToBeTyped = "1234"
    private val correctPasswordToBeTyped = "12341234"

    private fun forceClick(): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return allOf(isClickable(), isEnabled(), isDisplayed())
            }

            override fun getDescription(): String {
                return "force click"
            }

            override fun perform(uiController: UiController, view: View) {
                view.performClick() // perform click without checking view coordinates.
                uiController.loopMainThreadUntilIdle()
            }
        }
    }

    @get:Rule
    var activityRule=
        ActivityTestRule(AuthenticationActivity::class.java, false, true)

    @Before
    fun registerIdlingResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdlingResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun test_default_content_check(){

        onView(withId(R.id.welcome_title))
            .check(matches(isDisplayed()))

        onView(withId(R.id.welcome_description))
            .check(matches(isDisplayed()))

        onView(withId(R.id.text_message))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun test_invalid_credentials(){

        onView(withId(R.id.edit_text_email))
            .perform(typeText(emailToBeTyped), closeSoftKeyboard())

        onView(withId(R.id.edit_text_password))
            .perform(typeText(wrongPasswordToBeTyped), closeSoftKeyboard())

        onView(withId(R.id.button_confirm))
            .perform(click())

        onView(withId(R.id.text_message))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.invalid_credentials)))
    }


    /*
    @Test
    fun test_successful_login_and_start_new_activity(){

        IdlingPolicies.setMasterPolicyTimeout(120, TimeUnit.SECONDS)
        IdlingPolicies.setIdlingResourceTimeout(60, TimeUnit.SECONDS)

        val authenticationActivity = IntentsTestRule(AuthenticationActivity::class.java)
        authenticationActivity.launchActivity(Intent())

        onView(withId(R.id.edit_text_email))
            .perform(typeText(emailToBeTyped), closeSoftKeyboard())

        onView(withId(R.id.edit_text_password))
            .perform(typeText(correctPasswordToBeTyped), closeSoftKeyboard())

        onView(withId(R.id.button_confirm))
            .perform(click())

        onView(withId(R.id.parent_list))
            .perform(click())
    }
    */


}