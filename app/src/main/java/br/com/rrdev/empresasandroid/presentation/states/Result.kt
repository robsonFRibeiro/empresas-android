package br.com.rrdev.empresasandroid.presentation.states

import androidx.annotation.StringRes


sealed class Result {

    data class Success<T>(val data: T? = null): Result()
    data class Error(@StringRes val errorMessage: Int): Result()
    object InProgress: Result()

}


