package br.com.rrdev.empresasandroid.data.repository

import br.com.rrdev.empresasandroid.R
import java.lang.Exception

object ErrorFormatFactory {

    fun getMessageForCode(errorCode: Int): Int {
        return when(errorCode){
            401 -> R.string.invalid_credentials
            403 -> R.string.invalid_tokens
            500 -> R.string.error_server
            else-> R.string.unexpected_error
        }
    }

    fun getMessageForText(errorMessage: String?): Int {
        return when{
            errorMessage.isNullOrEmpty() ->R.string.unexpected_error
            errorMessage.contains("Unable to resolve host")->R.string.error_internet
            else->R.string.unexpected_error
        }
    }
}