package br.com.rrdev.empresasandroid.data.models

data class EnterprisesDataWrapper(val enterprises: List<Enterprise>)