package br.com.rrdev.empresasandroid.data.models

data class LogInDataWrapper(
//    val enterprise: Any,
    val investor: Investor,
    val success: Boolean
)