package br.com.rrdev.empresasandroid.di.module

import android.content.SharedPreferences
import br.com.rrdev.empresasandroid.BuildConfig
import br.com.rrdev.empresasandroid.data.repository.EnterpriseRepositoryImp
import br.com.rrdev.empresasandroid.domain.repository.EnterpriseRepository
import br.com.rrdev.empresasandroid.network.EnterpriseService
import br.com.rrdev.empresasandroid.presentation.viewmodels.AuthenticationViewModel
import br.com.rrdev.empresasandroid.presentation.viewmodels.EnterpriseListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val AppModule = module {

    viewModel { AuthenticationViewModel(get()) }

    viewModel { EnterpriseListViewModel(get(), get()) }

    single { createEnterpriseApiClient() }

    single { createEnterpriseRepository(get(), get()) }

}

internal fun createEnterpriseApiClient(): EnterpriseService {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(EnterpriseService::class.java)
}

fun createEnterpriseRepository(enterpriseService: EnterpriseService, sharedPreferences: SharedPreferences): EnterpriseRepository {
    return EnterpriseRepositoryImp(enterpriseService, sharedPreferences)
}



